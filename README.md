# Login Register Path


## Content of this file
---------------------

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Login Register Path makes it possible to find modules within your Drupal installation. It is allow to abdement in secure url like login, register, password, logout and profile at end for admin to make some changes in configuration.

Our goal is to make it easier to find and install modules for people new to Drupal and site builders. Developers will also find this valuable since it provides the composer commands to get the modules.

- For the description of the module visit: [https://www.drupal.org/project/login_register_path](https://www.drupal.org/project/login_register_path)
- To submit bug reports and feature suggestions or to track changes, visit: [hhttps://www.drupal.org/project/login_register_path?categories=All](hhttps://www.drupal.org/project/login_register_path?categories=All)


## Requirements

This module requires no modules outside of Drupal core.


## Installation

*If you intend to contribute to Login Register Path, skip this step and use the "Contributing" instructions instead*

Install with composer: `composer require drupal/login_register_path` then enable the module.


## Contributing

- Follow the [Git instructions](https://www.drupal.org/project/login_register_path/git-instructions
  ) to clone Login Register Path to your site


## Configuration

Navigate to Administration > Configuration > User interface > Login Register Path.

You can easly found the Login Register Path Settings section to update following thing according to your requirment
- Login Path
- Register Path
- Password Path
- Logout Path
- User Profile Path


## Maintainers

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
